package utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Coordinate {
    public static final Coordinate UP = new Coordinate(0, 1);
    public static final Coordinate DOWN = new Coordinate(0, -1);
    public static final Coordinate RIGHT = new Coordinate(1, 0);
    public static final Coordinate LEFT = new Coordinate(-1, 0);
    public static final List<Coordinate> dirs = Arrays.asList(UP, DOWN, RIGHT, LEFT);
    public int x;
    public int y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void add(Coordinate c) {
        this.x += c.x;
        this.y += c.y;
    }

    public int dist(Coordinate c) {
        return Math.abs(x - c.x) + Math.abs(y - c.y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return x == that.x && y == that.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
