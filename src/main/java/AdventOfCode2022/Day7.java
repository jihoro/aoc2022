package AdventOfCode2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.OptionalInt;
import java.util.Set;

public class Day7 {
    public static void main(String[] args) throws IOException {
        // build file system
        File root = new File("/");
        File current = root;
        List<String> lines = Files.readAllLines(Path.of("input.txt"));
        for (String s : lines) {
            if (s.startsWith("$")) {
                if (s.contains("cd")) {
                    if (s.contains("..")) {
//                        System.out.println("parent " + s);
                        current = current.parent;
//                        System.out.println("current " + current.name);

                    } else if (s.contains("/")) {
//                        System.out.println("root " + s);
                        current = root;
//                        System.out.println("current " + current.name);
                    } else {
//                        System.out.println("new " + s);
                        String[] split = s.split(" ");
//                        System.out.println(split[2]);

                        for (File f : current.subFiles) {
//                            System.out.println(f.name);
                            if (f.name.equals(split[2])) {
                                current = f;
                                break;
                            }
                        }
//                        System.out.println("current " + current.name);

                    }
                }
            }
            else if (s.startsWith("dir")){
                String[] split = s.split(" ");
                current.add(new File(split[1]));

            } else {
                String[] split = s.split(" ");
                current.add(new File(split[1], Integer.parseInt(split[0])));
            }
        }
        // part 1
//        System.out.println(root.subFiles.stream().map(f -> f.name).toList());
        System.out.println(root.getSize());
        System.out.println(sum(root));


        // part 2
        int neededSpace = 30000000 - (70000000 - root.getSize());
        System.out.println(findFileToDelete(root, neededSpace, root.getSize()));
    }

    private static int findFileToDelete(File root, int neededSpace, int currentMin) {
        if (root.getSize() > neededSpace && root.getSize() < currentMin) {
            currentMin = root.getSize();
        }
        int finalCurrentMin = currentMin;
        OptionalInt nextMin = root.subFiles.stream().filter(f -> f.isDirectory)
                .mapToInt(file -> findFileToDelete(file, neededSpace, finalCurrentMin))
                .min();

        if (nextMin.isPresent() && currentMin > nextMin.getAsInt()) {
            currentMin = nextMin.getAsInt();
        }
        return currentMin;
    }

    private static int sum(File root) {
//        System.out.println(root.name + " "+  root.getSize() + " " + root.subFiles.stream().map(file -> file.name).toList());
        return (root.getSize() >= 100000 ? 0 : root.getSize()) + root.subFiles.stream()
                .filter(f -> f.isDirectory)
                .mapToInt(Day7::sum)
                .sum();
    }

    static class File {
        int size;
        boolean isDirectory;
        Set<File> subFiles = new HashSet<>();
        File parent;
        String name;

        public File(String name, int size) {
            this.name = name;
            this.size = size;
            isDirectory = false;
        }

        public File (String name) {
            this.name = name;
            isDirectory = true;
        }

        public void add(File file) {
            subFiles.add(file);
            file.parent = this;
        }

        public int getSize() {
            if (this.isDirectory) {
                return subFiles.stream().mapToInt(File::getSize).sum();
            }
            return size;
        }
    }

}
