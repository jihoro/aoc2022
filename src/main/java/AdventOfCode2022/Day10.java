package AdventOfCode2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class Day10 {
    static int x = 1;
    static int result = 0;
    static int cycle = 0;

    public static void main(String[] args) throws IOException {

        List<String> list = Files.readAllLines(Path.of("input.txt"));
        for(String s : list) {
            if (s.contains("noop")) {
                cycle();
            } else {
                cycle();
                cycle();
                x += Integer.parseInt(s.split(" ")[1]);
            }
        }
        System.out.println(result);
    }

    private static void cycle() {
        cycle++;
        int sprite = cycle % 40 == 0 ? 40 : cycle % 40;
        if (sprite >= x && sprite <= x + 2) {
            System.out.print("#");
        } else {
            System.out.print(" ");
        }
        if (cycle % 40 == 0)
            System.out.println();
        if ((cycle - 20) % 40 == 0) {
            result += cycle * x;
//            System.out.println(result);
        }
    }
}
