package AdventOfCode2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class Day11 {
    public static void main(String[] args) throws IOException {
        List<String> list = Files.readAllLines(Path.of("input.txt"));
        List<String> ops = new ArrayList<>();
        List<Integer> tests = new ArrayList<>();
        List<String> conditions =  new ArrayList<>();
        for (int i = 2; i < list.size(); i += 7) {
            ops.add(list.get(i).substring(23));
        }
        for (int i = 3; i < list.size(); i += 7) {
            tests.add(Integer.parseInt(list.get(i).substring(21)));
        }
        // part 2 for loop added
        int modulo = 1;
        for (int i : tests) {
            modulo *= i;
        }
        for (int i = 4; i < list.size(); i += 7) {
            conditions.add(list.get(i).substring(29));
            conditions.add(list.get(i + 1).substring(30));
        }
        List<List<Long>> items = new ArrayList<>();
        for (int i = 1; i < list.size(); i += 7) {
            List<Long> perMonkey = new ArrayList<>();
            for (String x  : list.get(i).substring(18).split(", ")) {

                perMonkey.add(Long.parseLong(x));
            }
            items.add(perMonkey);
        }
        int[] numInspections = new int[ops.size()];
        System.out.println(modulo);

        for (int x = 0; x < 10000; x++) {
            for (int i = 0; i < numInspections.length; i++) {
                for (int j = 0; j < items.get(i).size(); j++) {
                    long current  = items.get(i).get(j);
//                    System.out.println(current);
                    if (ops.get(i).equals("* old")) {
                        current *= current;
                    } else if (ops.get(i).contains("* ")) {
                        current *= Integer.parseInt(ops.get(i).substring(2));
                    } else {
                        current += Integer.parseInt(ops.get(i).substring(2));
                    }
                    current = current % modulo ; // for part 1, current /3
                    if (current % tests.get(i) == 0) {
                        items.get(Integer.parseInt(conditions.get(i * 2))).add(current);
                    } else {
                        items.get(Integer.parseInt(conditions.get(i * 2 + 1))).add(current);
                    }
                    numInspections[i] += 1;
                }
                items.set(i, new ArrayList<>());
            }
            for (int i = 0; i < numInspections.length; i++) {
//                System.out.println(items.get(i));
            }

        }
        System.out.println(Arrays.toString(Arrays.stream(numInspections).boxed().sorted(Comparator.reverseOrder()).limit(2).toArray()));//reduce(1, (a, b) -> a * b));
    }
}
