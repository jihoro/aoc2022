package AdventOfCode2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class Day5 {

    public static void main(String[] args) throws IOException {
        // part 1
        ArrayList<Stack<Integer>> list = new ArrayList<>();
        ArrayList<String> input = new ArrayList<>(Arrays.asList(
                "FCPGQR",
                "WTCP",
                "BHPMC",
                "LTQSMPR",
                "PHJZVGN",
                "DPJ",
                "LGPZFJTR",
                "NLHCFPTJ",
                "GVZQHTCW"
        ));
        input.forEach(s -> {
            Stack<Integer> stack = new Stack<>();
            s.chars().forEach(stack::push);
            list.add(stack);
        });

        Files.readAllLines(Path.of("input.txt")).stream().forEach(s -> {
            s = s.replaceAll("\\D+", " ");
            String[] nums = s.trim().split("\\D+");
            System.out.println(Arrays.toString(nums));
            int from = Integer.parseInt(nums[1]) - 1;
            int to = Integer.parseInt(nums[2]) - 1;
//            System.out.println(from + " "+  to);
            for (int i = 0; i < Integer.parseInt(nums[0]); i++) {
                if (!list.get(from).isEmpty())
                    list.get(to).push(list.get(from).pop());
            }
            for (Stack<Integer> stack : list) {
                for(int i : stack)
                    System.out.print((char)i);
                System.out.println();
            }

        });
        StringBuilder result = new StringBuilder();
        for (Stack<Integer> s : list) {
            if (!s.isEmpty())
                result.append((char)(int)s.peek());
            else
                result.append(" ");
        }
        System.out.println(result);

        // part 2
        ArrayList<Deque<Integer>> list2 = new ArrayList<>();
        ArrayList<String> input2 = new ArrayList<>(Arrays.asList(
                "FCPGQR",
                "WTCP",
                "BHPMC",
                "LTQSMPR",
                "PHJZVGN",
                "DPJ",
                "LGPZFJTR",
                "NLHCFPTJ",
                "GVZQHTCW"
        ));
        input2.forEach(s -> {
            Deque<Integer> stack = new ArrayDeque<>();
            s.chars().forEach(stack::offerLast);
            list2.add(stack);
        });

        Files.readAllLines(Path.of("input.txt")).stream().forEach(s -> {
            s = s.replaceAll("\\D+", " ");
            String[] nums = s.trim().split("\\D+");
//            System.out.println(Arrays.toString(nums));
            int from = Integer.parseInt(nums[1]) - 1;
            int to = Integer.parseInt(nums[2]) - 1;
//            System.out.println(from + " "+  to);
            ArrayList<Integer> toMove = new ArrayList<>();
            for (int i = 0; i < Integer.parseInt(nums[0]); i++) {
                if (!list2.get(from).isEmpty())
                    toMove.add(list2.get(from).pollLast());
            }
            for (int i = toMove.size() - 1; i >= 0; i--) {
                list2.get(to).offerLast(toMove.get(i));
            }
            for (Deque<Integer> stack : list2) {
                for(int i : stack)
                    System.out.print((char)i);
                System.out.println();
            }

        });
        StringBuilder result2 = new StringBuilder();
        for (Deque<Integer> s : list2) {
            if (!s.isEmpty())
                result2.append((char)(int)s.peekLast());
            else
                result2.append(" ");
        }
        System.out.println(result2);

    }
}
