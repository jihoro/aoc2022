package AdventOfCode2022;


import utils.Coordinate;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Day9 {
    public static void main(String[] args) throws IOException {

        // part 1
        Coordinate head = new Coordinate(0, 0);
        Coordinate tail = new Coordinate(0, 0);
        HashSet<Coordinate> set = new HashSet<>();
        for (String s : Files.readAllLines(Path.of("input.txt"))) {
            String[] split = s.split(" ");
            Coordinate direction = null;
            switch (split[0]) {
                case "R" -> direction = Coordinate.RIGHT;
                case "L" -> direction = Coordinate.LEFT;
                case "U" -> direction = Coordinate.UP;
                case "D" -> direction = Coordinate.DOWN;
            }
            for (int i = 0; i < Integer.parseInt(split[1]); i++) {
                head.add(direction);
                int dist = tail.dist(head);
                if ((head.x == tail.x || head.y == tail.y) && dist > 1) {
                    if (head.x == tail.x)
                        tail.y += Math.signum(head.y - tail.y);
                    else
                        tail.x += Math.signum(head.x - tail.x);
                } else if (dist > 2) {
                    tail.y += Math.signum(head.y - tail.y);
                    tail.x += Math.signum(head.x - tail.x);
                }
                set.add(new Coordinate(tail.x, tail.y));
            }
        }
        System.out.println(set.size());

        // part 2
        List<Coordinate> nodeList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            nodeList.add(new Coordinate(0, 0));
        }
        HashSet<Coordinate> set2 = new HashSet<>();
        Coordinate tail2 = null;
        for (String s : Files.readAllLines(Path.of("input.txt"))) {
            String[] split = s.split(" ");
            Coordinate direction = null;
            switch (split[0]) {
                case "R" -> direction = Coordinate.RIGHT;
                case "L" -> direction = Coordinate.LEFT;
                case "U" -> direction = Coordinate.UP;
                case "D" -> direction = Coordinate.DOWN;
            }
            for (int i = 0; i < Integer.parseInt(split[1]); i++) {
                nodeList.get(0).add(direction);
                for (int j = 0; j < 9; j++) {
                    Coordinate head2 = nodeList.get(j);
                    tail2 = nodeList.get(j + 1);
                    int dist = tail2.dist(head2);
                    if ((head2.x == tail2.x || head2.y == tail2.y) && dist > 1) {
                        if (head2.x == tail2.x)
                            tail2.y += Math.signum(head2.y - tail2.y);
                        else
                            tail2.x += Math.signum(head2.x - tail2.x);
                    } else if (dist > 2) {
                        tail2.y += Math.signum(head2.y - tail2.y);
                        tail2.x += Math.signum(head2.x - tail2.x);
                    }
                }
                set2.add(new Coordinate(tail2.x, tail2.y));
            }
        }
        System.out.println(set2.size());

    }
}
