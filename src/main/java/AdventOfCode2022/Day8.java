package AdventOfCode2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class Day8 {
    public static void main(String[] args) throws IOException {
        List<String> input = Files.readAllLines(Path.of("input.txt"));
        int[][] grid = new int[input.size()][input.get(0).length()];

        for (int i = 0; i < input.size(); i++) {
            String s = input.get(i);
            for (int j = 0; j < s.length(); j++) {
                grid[i][j] = Character.getNumericValue(s.charAt(j));
            }
        }
        // part 1
        int part1 = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (visible(grid, i ,j)) {
                    part1++;
                }
            }
        }

        System.out.println(part1);

        // part 2
        int part2 = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                int curr = scenicScore(grid, i , j);
                if (curr > part2) {
                    part2 = curr;
                }
            }
        }
        System.out.println(part2);
    }

    private static int scenicScore(int[][] grid, int i, int j) {
        int up = 0;
        for (int a = i - 1; a >= 0; a--) {
            up++;
            if (grid[a][j] >= grid[i][j]) {
                break;
            }
        }

        int down = 0;
        for (int a = i + 1; a < grid.length; a++) {
            down++;
            if (grid[a][j] >= grid[i][j] ) {
                break;
            }
        }

        int left = 0;
        for (int a = j - 1; a >= 0; a--) {
            left++;
            if (grid[i][a] >= grid[i][j] ) {
                break;
            }
        }

        int right = 0;
        for (int a = j + 1; a < grid[0].length; a++) {
            right++;
            if (grid[i][a] >= grid[i][j] ) {
                break;
            }
        }
        return up * down * left * right;

    }

    private static boolean visible(int[][] grid, int i, int j) {
        boolean visible = true;
        for (int a = 0; a < i; a++) {
            if (grid[a][j] >= grid[i][j]) {
                visible = false;
                break;
            }
        }
        if (visible) {
            return true;
        }
        visible = true;
        for (int a = i + 1; a < grid.length; a++) {
            if (grid[a][j] >= grid[i][j]) {
                visible = false;
                break;
            }
        }
        if (visible) {
            return true;
        }

        visible = true;
        for (int a = 0; a < j; a++) {
            if (grid[i][a] >= grid[i][j]) {
                visible = false;
                break;
            }
        }
        if (visible) {
            return true;
        }

        visible = true;
        for (int a = j + 1; a < grid[0].length; a++) {
            if (grid[i][a] >= grid[i][j]) {
                visible = false;
                break;
            }
        }
        return visible;
    }
}
