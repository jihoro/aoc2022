package AdventOfCode2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

public class Day2 {
    public static void main(String[] args) throws IOException {
        /* just an idea for different approach
        Map<String, Integer> map = Map.of(
                                    "A X", 3,
                                    "A Y", 4,
                                    "A Z", 8,
                                    "B X", 1,
                                    "B Y", 5,
                                    "B Z", 9,
                                    "C X", 2,
                                    "C Y", 6,
                                    "C Z", 7
                                        );
        /*
         */
        // part 1
        int total = Files.readAllLines(Path.of("input.txt")).stream().mapToInt(
                s -> {
                    int opponent = s.charAt(0) - 'A';
                    int me = s.charAt(2) - 'X';
                    int round = (me - opponent + 4) % 3 * 3 + me + 1;
                    return round;
                }
        ).sum();
        System.out.println(total);


        // part 2
        Map<String, Integer> map = Map.of(
                "A X", 3,
                "A Y", 4,
                "A Z", 8,
                "B X", 1,
                "B Y", 5,
                "B Z", 9,
                "C X", 2,
                "C Y", 6,
                "C Z", 7
        );
        int newTotal = Files.readAllLines(Path.of("input.txt")).stream().mapToInt(
                s -> {
                    return map.get(s);
//                    int opponent = s.charAt(0) - 'A';
//                    int me = (opponent + (s.charAt(2) - 'X') + 2) % 3;
//                    int round = (me - opponent + 4) % 3 * 3 + me + 1;
//                    return round;
                }
        ).sum();
        System.out.println(newTotal);
    }
}

