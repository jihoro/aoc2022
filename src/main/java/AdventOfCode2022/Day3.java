package AdventOfCode2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class Day3 {
    public static void main(String[] args) throws IOException {
        // part 1
        int total = Files.readAllLines(Path.of("input.txt")).stream().mapToInt(
                s -> {
                    int mid = s.length() / 2;
                    String first = s.substring(0, mid);
                    String second = s.substring(mid);
                    char duplicate = 0;
                    for (char c : first.toCharArray()) {
                        if (second.indexOf(c) != -1) {
                            duplicate = c;
                        }
                    }
                    return Character.isUpperCase(duplicate) ? duplicate - 38 : duplicate - 96;
                }
        ).sum();

        System.out.println(total);

        // part 2
        List<String> list = Files.readAllLines(Path.of("input.txt"));
        int sum = 0;
        for (int i = 0; i < list.size(); i+=3) {
            String s1 = list.get(i);
            String s2 = list.get(i + 1);
            String s3 = list.get(i + 2);

            for (char c : s1.toCharArray()) {
                if (s2.indexOf(c) != -1 && s3.indexOf(c) != -1) {
                    sum += Character.isUpperCase(c) ? c - 38 : c - 96;
                    break;
                }
            }

        }
        System.out.println(sum);
    }
}
