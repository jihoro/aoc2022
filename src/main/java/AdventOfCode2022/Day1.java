package AdventOfCode2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.IntStream;

public class Day1 {
    public static void main(String[] args) throws IOException {
        System.out.println(System.getProperty("user.dir"));
        IntStream s = Arrays.stream(Files.readString(Path.of("input.txt")).split("\n\n"))
                .map(x -> Arrays.stream(x.split("\n"))
                        .mapToInt(y -> Integer.parseInt(y))
                        .sum()
                )
                .sorted(Comparator.reverseOrder())
                .mapToInt(Integer::intValue);
        // part 1
//        System.out.println(s.limit(1).sum());
        // part 2
        System.out.println(s.limit(3).sum());
    }
}
