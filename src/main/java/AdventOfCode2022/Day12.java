package AdventOfCode2022;

import utils.Coordinate;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class Day12 {
    public static void main(String[] args) throws IOException {
        ArrayList<ArrayList<Character>> input = new ArrayList<>();
        int sx = 0,sy = 0,ex = 0,ey = 0;

        for (String s : Files.readAllLines(Path.of("input.txt"))) {
            ArrayList<Character> list = new ArrayList<>();
            for (char c : s.toCharArray()) {
                list.add(c);
                if (c == 'S') {
                    sx = input.size();
                    sy = s.indexOf("S");
                }
                if (c == 'E') {
                    ex = input.size();
                    ey = s.indexOf("E");
                }
            }
            input.add(list);
        }
        int n = input.size(), m = input.get(0).size();
        input.get(sx).set(sy, 'a');
        input.get(ex).set(ey, 'z');
        Coordinate start = new Coordinate(sx, sy);
        Coordinate end = new Coordinate(ex, ey);

        int part = 2;

        Deque<Coordinate> queue = new ArrayDeque<>();
        queue.offerLast(start);

        if (part == 2) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    if (input.get(i).get(j) == 'a') {
                        Coordinate c = new Coordinate(i, j);
                        queue.offerLast(c);
                    }
                }
            }
        }

        HashMap<Coordinate, Integer> dist = new HashMap<>();
        for (Coordinate c : queue) {
            dist.put(c, 0);
        }
        while (!queue.isEmpty()) {
            Coordinate curr = queue.pollFirst();
            if (curr.equals(end)) {
                System.out.println(dist.get(curr));
                return;
            }
            for (Coordinate c : Coordinate.dirs) {
                int cx = c.x + curr.x, cy = c.y + curr.y;
                Coordinate add = new Coordinate(cx, cy);

                if (cx >= 0 && cx < input.size() && cy >= 0 && cy < input.get(0).size()) {
//                    System.out.println("trying " + cx + " " + cy);
//                    System.out.println("current is " + curr.x + " " + curr.y);
                    if (input.get(curr.x).get(curr.y) + 1 >= input.get(cx).get(cy)) {
//                        System.out.println((char)input.get(curr.x).get(curr.y) + " " + (char)input.get(cx).get(cy));
                        int newDist = dist.get(curr) + 1;
//                        System.out.println(newDist);
                        if (newDist < dist.getOrDefault(add, Integer.MAX_VALUE)) {
//                            System.out.println("Adding to Queue " + cx + " " + cy);
//                            System.out.println("distance is " + newDist + " for " + input.get(cx).get(cy));
//                            System.out.println();
                            queue.offerLast(add);
                            dist.put(add, newDist);
                        }
                    }
                }
            }

        }

    }
}
