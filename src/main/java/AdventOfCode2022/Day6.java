package AdventOfCode2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Day6 {
    public static void main(String[] args) throws IOException {
        String input = Files.readString(Path.of("input.txt"));
        // part 1
        int part1 = 0;
        for (int i = 0; i <= input.length() - 4; i++) {
            if (input.substring(i, i + 4).chars().distinct().count() == 4) {
                part1 = i + 4;
                break;
            }
        }
        System.out.println(part1);

        // part 2
        int part2 = 0;
        for (int i = 0; i <= input.length() - 14; i++) {
            if (input.substring(i, i + 14).chars().distinct().count() == 14) {
                part2 = i + 14;
                break;
            }
        }

        System.out.println(part2);
    }
}
