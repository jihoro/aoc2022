package AdventOfCode2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

public class Day4 {
    public static void main(String[] args) throws IOException {
        // part 1
        int result = Files.readAllLines(Path.of("input.txt")).stream().mapToInt(
                s -> {
                    int[] split = Arrays.stream(s.split("[,-]")).mapToInt(Integer::parseInt).toArray();
                    if (split[0] <= split[2] && split[1] >= split[3] ||
                    split[0] >= split[2] && split[1] <= split[3]) {
                        return 1;
                    }
                    return 0;
                }
        ).sum();

        System.out.println(result);

        // part 2
        int result2 = Files.readAllLines(Path.of("input.txt")).stream().mapToInt(
                s -> {
                    int[] split = Arrays.stream(s.split("[,-]")).mapToInt(Integer::parseInt).toArray();
                    if (split[0] <= split[3] && split[0] >= split[2] ||
                            split[2] >= split[0] && split[2] <= split[1]) {
                        return 1;
                    }
                    return 0;
                }
        ).sum();

        System.out.println(result2);
    }
}
